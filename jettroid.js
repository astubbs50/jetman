"use strict";

var canvas;
var context;
var canvasUI;
var contextUI;
	
var allObjects;
var collisionLayers;
var staticLayer = 0;
var dynamicLayer = 1;
var triggers;
var trackers;
var player;
var dt;
var gravity;
var lastTime;
var lastId;
var airFriction = 2;
var camera;
var tileSpriteSheet;
var backgroundImage;
var backgroundObjects = [];
var foregroundObjects = [];
var imagesLoaded = {};
var inventory = [];
var updateInventory = true;
var selectedItem = 0;
var cameraMaxX = 600;
var cameraMinX = 200;
var cameraMaxY = 500;
var cameraMinY = 100;
var aspectX = 16;
var aspectY = 9;
var gameX = 1366;
var gameY = 768;
var mapObjects = {};
var timeout;
var showingMessage = false;
var startMessage = "";
var sounds = ["sounds/engine5.wav", "sounds/walking4.wav", "sounds/shock.wav", "sounds/item.wav", "sounds/open.wav", "sounds/message.wav"];
var audioPlayers = [];

function init() {	
	canvas = document.getElementById("myCanvas");	
	context = canvas.getContext("2d");
	canvasUI = document.getElementById("myCanvasUI");
	contextUI = canvasUI.getContext("2d");
	
	context.imageSmoothingEnabled = false;
	contextUI.imageSmoothingEnabled = false;	
	resizeCanvas();
	context.fillStyle = "white";	
	backgroundImage = new Image();
	backgroundImage.src = "img/level_01_background_04.png";	
	checkLoaded(backgroundImage.src, backgroundImage);
	initGame();
	//loadLevel(0);
	loadLevel("jet_level1");
		
	waitForImages();
	
	window.onresize = resizeCanvas;	
	for(var i = 0; i < sounds.length; i++) {
		createAudioPlayer(sounds[i]);
	}
	//run();
}

function resizeCanvas() {
	var canvasHeight;
	var canvasWidth;
	
	if(window.innerWidth > window.innerHeight) {
		var aspect = aspectY / aspectX;
		canvasHeight = window.innerWidth * aspect;
		canvasWidth = window.innerWidth;
		
		canvasUI.style.height = window.innerHeight + "px";
		canvasUI.style.width = (window.innerHeight * (aspectX / aspectY)) + "px";		
		canvasUI.style.left = (window.innerWidth - (window.innerHeight * (aspectX / aspectY))) / 2 + "px";
	} else {
		var aspect = aspectX / aspectY;
		canvasHeight = window.innerHeight;
		canvasWidth = window.innerHeight * aspect;
		
		canvasUI.style.height = (window.innerWidth * (aspectY / aspectX)) + "px";
		canvasUI.style.width = window.innerWidth + "px";
		canvasUI.style.top = "0px";
	}
	
	var diffY = ((canvasHeight - window.innerHeight) / 2);
	var diffGameY = diffY * (gameY / canvasHeight);
	cameraMaxY = gameY - (diffGameY + 200);
	cameraMinY = (diffGameY + 200);	
	canvas.style.height = canvasHeight + "px";
	canvas.style.top = -diffY + "px";	

	var diffX = ((canvasWidth - window.innerWidth) / 2);
	var diffGameX = diffX * (gameX / canvasWidth);
	cameraMaxX = gameX - (diffGameX + 400);
	cameraMinX = (diffGameX + 400);	
	canvas.style.width = canvasWidth + "px";
	canvas.style.left = -diffX + "px";
	
	var canvasBack = document.getElementById("myCanvasBack");
	canvasBack.style.height = canvasHeight + "px";
	canvasBack.style.width = canvasWidth + "px";	
	canvasBack.style.left = -diffX + "px";
	canvasBack.style.top = -diffY + "px";	
}

function waitForImages() {
	var allLoaded = true;
	for(var i in imagesLoaded) {
		if(!imagesLoaded[i]) {
			allLoaded = false;
		}
	}
	
	if(allLoaded) {
		var canvasBack = document.getElementById("myCanvasBack");
		var contextBack = canvasBack.getContext("2d");
		contextBack.drawImage(backgroundImage, 0, 0, canvasBack.width, canvasBack.height);		
		run();
		run();
		if(startMessage.length > 0) {
			showMessage(startMessage, false);
		}
	} else {
		setTimeout(waitForImages, 1000);
	}
}

function createAudioPlayer(soundFile) {
	var audioId = "ap_" + (audioPlayers.length);
	var audioSrc = soundFile;
	var audioElement = document.createElement('audio');
	audioElement.id = audioId;
	audioElement.setAttribute('src', audioSrc);
	audioElement.setAttribute('preload', 'auto');
	audioElement.volume = 0.5;
	document.body.appendChild(audioElement);
	audioPlayers.push(audioElement);
	
	return audioId;				
}

function initGame() {
	camera = {x: 100, y: 0};
	lastId = 0;
	allObjects = [];
	collisionLayers = [[],[]];
	triggers = [];
	trackers = [];
	lastTime = (new Date).getTime();
	dt = 0;
	gravity = 0.0005;	
	
	//Init player
	var spriteSheet = new Image();
	spriteSheet.src = "img/spirite_8_17.png";
	checkLoaded(spriteSheet.src, spriteSheet);
	spriteSheet.spriteSize = { x: 66, y: 66, framesPerRow: 11 };
	player = createObj(10, 10, 30, 84, drawSprite, "dynamic", spriteSheet, 100, 100, -33, -10);	
	player.animationFrame = 0;
	player.animationIdle = [7, 7, 7, 8, 8, 8];
	player.animationFalling = [7];
	player.animationFlying = [26, 27];
	player.animationFlyingForward = [24, 25];
	player.animationWalking = [0, 1, 2, 3, 4, 5, 6];
	player.animationRunning = [33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55];
	player.animationDeath = [11, 12, 13, 14, 15, 16, 17, 18];
	player.animationDead = [18];
	player.animation = player.animationIdle;
	player.animationDelay = 100;
	player.animationDelayNormal = 100;
	player.animationDelayFast = 30;
	player.animationLast = lastTime;
	player.animationDirection = 1;
	player.animationRepeat = true;
	player.fuel = 100;
	player.oxygen = 100;
	player.oxygenConsumption = 0.0001;
	player.mainFuelConsumption = 0.001;
	player.secondaryFuelConsumption = 0.0001;
	player.dead = false;
	player.justDied = false;
	
	//Create the grounded trigger
	var groundedTrigger = createTrigger(0, 0, player.width / 4, 3, function () {
		this.tracker.grounded = true;
		this.tracker.speed = this.tracker.normalSpeed;
	}, function () {
		this.tracker.grounded = false;
		this.tracker.speed = this.tracker.flightSpeed;
	});
	
	setTracker(groundedTrigger, player, player.width / 3, player.height + 4, function () {
		this.x = this.tracker.x + this.relX;
		this.y = this.tracker.y + this.relY;
	});	
	
	var testTrigger = createTrigger(0, 0, 3, 3, function () { 
		this.color = "green";
	}, function () {
		this.color = "white";
	});
}

function loadLevel(name) {
	var blockSize = 128;
	var level = TileMaps[name];
	var sourceWidth = 64;
	var sourceHeight = 64;	
	var spacing = 2;	
	
	var columns = 8;
	for(var i = 0; i < level.tilesets.length; i++) {
		if(level.tilesets[i].name === "tiles") {
			tileSpriteSheet = new Image();
			tileSpriteSheet.src = level.tilesets[i].image;
			checkLoaded(tileSpriteSheet.src, tileSpriteSheet);
			columns = level.tilesets[i].columns;
			sourceWidth = level.tilesets[i].tilewidth;
			sourceHeight = level.tilesets[i].tileheight;	
			spacing = level.tilesets[i].spacing;
		}
	}
	var offset = spacing / 2;
	for(var i = 0; i < level.layers.length; i++) {			
		if(level.layers[i].type === "tilelayer") {	
			var strTileType = "background";
			var mapWidth = level.layers[i].width;
			if(level.layers[i].properties && level.layers[i].properties["IsCollision"]) {
				strTileType = "static";
			}
			
			for(var j = 0; j < level.layers[i].data.length; j++) {
				if(level.layers[i].data[j] > 0) {
					
					var gid = level.layers[i].data[j] - 1;					
					var sourceOffset = getOffset(gid, columns, sourceWidth + spacing, sourceHeight + spacing);
					var destOffset = getOffset(j, mapWidth, blockSize, blockSize, 0);					
					createObj(destOffset.x, destOffset.y, blockSize + 1, blockSize + 1, drawImage, strTileType, tileSpriteSheet, sourceWidth, sourceHeight, sourceOffset.x + offset, sourceOffset.y + offset);
				}
			}
		} else if(level.layers[i].type === "objectgroup") {
			for(var j = 0; j < level.layers[i].objects.length; j++) {
				var blockDiff = blockSize / sourceWidth;
				
				var obj = level.layers[i].objects[j];
				
				obj.x *= blockDiff;
				obj.y *= blockDiff;
				obj.width *= blockDiff;
				obj.height *= blockDiff;
				
				if(obj.name === "Start") {
					player.x = obj.x + obj.width / 2;
					player.y = obj.y + obj.height / 2;
				}
				if(obj.type === "Message") {
					setMessageObject(obj);
				}
				if(obj.type === "Enemy") {
					if(obj.properties && obj.properties["EnemyType"]) {
						var et = obj.properties["EnemyType"];
						if(et === "Sparky") {							
							createSparky(obj);
						}
					}						
				}
				
				if(obj.gid) {
					var strTileType = "background";
					if(obj.properties && obj.properties["IsCollision"]) {
						strTileType = "static";
					}
					
					var sourceOffset = getOffset(obj.gid - 1, columns, sourceWidth + spacing, sourceHeight + spacing);
					var backgroundObj = createObj(obj.x, obj.y - obj.height, obj.width + 1, obj.height + 1, drawImage, strTileType, tileSpriteSheet, sourceWidth, sourceHeight, sourceOffset.x + offset, sourceOffset.y + offset);
					obj.gameObj = backgroundObj;
					if(obj.properties && obj.properties["IsItem"]) {
						setPickupItemTrigger(obj, backgroundObj, columns);
					}
				}
				
				if(obj.properties && obj.properties["Start Message"]) {
					startMessage = obj.properties["Start Message"];
				}
				mapObjects[obj.name] = obj;
			}
		}
	}
	
	setCollisionResponses();
	
	var startItem = {
		 "gid":12,
		 "height":15,
		 "id":9,
		 "name":"Torch",                 
		 "rotation":0,
		 "type":"Torch",
		 "visible":true,
		 "width":15,
		 "x":206,
		 "y":383
	};
	
	addToInventory(startItem, columns);
}

function createSparky(obj) {
	var sparkImage = new Image();
	sparkImage.src = "img\/tiles\/star.png";
	checkLoaded(sparkImage.src, sparkImage);
	var gameObj = createObj(obj.x, obj.y, obj.width, obj.height, drawParticles, "background");
	gameObj.img = sparkImage;	
	createParticleSystem(gameObj);	
	obj.gameObj = gameObj;
	
	//Explosion
	/*
	var spriteSheet = new Image();
	spriteSheet.src = "img/spirite_8_16.png";
	checkLoaded(spriteSheet.src, spriteSheet);
	spriteSheet.spriteSize = { x: 66, y: 66, framesPerRow: 11 };
	player = createObj(10, 10, 30, 84, drawSprite, "dynamic", spriteSheet, 100, 100, -33, -14);	
	*/
	var explosionSheet = new Image();
	explosionSheet.src = "img/explosion.png";
	checkLoaded(explosionSheet.src, explosionSheet);
	explosionSheet.spriteSize = { x: 34, y: 34, framesPerRow: 3 };
	
	var sparkyTrigger = createTrigger(obj.x, obj.y, obj.width, obj.height, function () {
		//showMessage("Sparks!!!!");
		player.dead = true;
		player.justDied = true;		
		var explosion = createObj(player.x, player.y, player.width / 2, player.height / 2, drawSprite, "foreground", explosionSheet, 64, 64, -32, -32);
		explosion.animation = [0, 1, 2, 3, 4, 3, 2, 1, 0, 5];
		explosion.animationFrame = 0;
		explosion.animationLast = 0;
		explosion.animationDelay = 50;
		explosion.animationRepeat = false;
		setTracker(explosion, player, player.width / 2, player.height / 2, function () {
			this.x = this.tracker.x + this.relX;
			this.y = this.tracker.y + this.relY;
		});	
	}, function () {
	});
}

function createParticleSystem(obj, properties) {
	var defaults = {
		count: 15,
		maxSize: 13,
		minSize: 5,
		maxX: obj.width,
		minX: 0,
		maxY: obj.height,
		minY: 0,
		maxVx: 0.03,
		minVx: -0.03,
		maxVy: -0.1,
		minVy: -0.2,
		maxLife: 1000,
		minLife: 500,
		maxAngle: Math.PI * 2,
		minAngle: 0,
		minSpin: 0,
		maxSpin: Math.PI / 25,
		colorChoices: ["#FF4500", "#FF8C00", "#FFFF00"]
	};
	var opt = {};
	$.extend(opt, defaults, properties);		
	
	obj.particles = [];
	for(var k = 0; k < opt.count; k++) {
		var pt = {
			reset: function () {
				var size = getRndRange(opt.minSize, opt.maxSize);
				this.x = obj.x + getRndRange(opt.minX, opt.maxX);
				this.y = obj.y + getRndRange(opt.minY, opt.maxY); 
				this.vx = getRndRange(opt.minVx, opt.maxVx);
				this.vy = getRndRange(opt.minVy, opt.maxVy);
				this.color = opt.colorChoices[Math.floor(Math.random() * opt.colorChoices.length)];
				this.life = getRndRange(opt.minLife, opt.maxLife);
				this.angle = getRndRange(opt.minAngle, opt.maxAngle);
				this.spin = getRndRange(opt.minSpin, opt.maxSpin);
				this.width = size;
				this.height = size;
			},																
			width: opt.size,
			height: opt.size,
			angle: 0,
			move: moveObject,	
			maxLife: opt.maxLife
		};					
		pt.reset();
		obj.particles.push(pt);
	}
}

function getRndRange(min, max) {
	return Math.random() * (max - min) + min;
}

function setPickupItemTrigger(obj, backgroundObj, columns) {
	
	//Create Pick up item trigger	
	var itemTrigger = createTrigger(obj.x, obj.y - obj.height, obj.width, obj.height, function () {
		//this.color = "green";
		//Remove trigger
		audioPlayers[3].play();
		
		triggers.splice(triggers.indexOf(this), 1);
		
		//Remove the background object
		backgroundObjects.splice(backgroundObjects.indexOf(backgroundObj), 1);
		
		addToInventory(obj, columns);
		
		if(obj.properties && obj.properties["Pickup Message"]) {
			drawInventory();
			showMessage(obj.properties["Pickup Message"], obj.properties["Important Message"]);
			audioPlayers[0].currentTime = 0;
			audioPlayers[0].pause();
			audioPlayers[1].currentTime = 0;
			audioPlayers[1].pause();	
			audioPlayers[2].currentTime = 0;
			audioPlayers[2].pause();			
		}
		
		if(obj.properties && obj.properties["After Pickup"]) {
			var cmd = obj.properties["After Pickup"].split(" ");
			if(cmd[0] === "Update") {
				var objToUpdate = mapObjects[cmd[1]];
				var triggerToUpdate = objToUpdate.gameObj;
				if(triggers.indexOf(triggerToUpdate) === -1) {
					triggers.push(triggerToUpdate);
				}
				objToUpdate.properties["MsgIndex"]++;
				objToUpdate.properties["Message0"] = objToUpdate.properties["Message" + objToUpdate.properties["MsgIndex"]];
			}
		}
	}, function () {
		//this.color = "white";
	});	
}

function setMessageObject(obj) {
	
	//Create Pick up item trigger	
	var itemTrigger = createTrigger(obj.x, obj.y, obj.width, obj.height, function () {
		//this.color = "green";
		//Remove trigger
		triggers.splice(triggers.indexOf(this), 1);
						
		if(obj.properties && obj.properties["Message0"]) {			
			showMessage(obj.properties["Message0"], obj.properties["Important Message"]);
		}
	}, function () {
		//this.color = "white";
	}, [[player]]);	
	obj.gameObj = itemTrigger;
}

function addToInventory(obj, columns) {
	var sourceOffset = getOffset(obj.gid - 1, columns, 66, 66);
	var item = {
		obj: obj,
		img: tileSpriteSheet,
		sourceWidth: 64,
		sourceHeight: 64,
		offsetX: sourceOffset.x + 1,
		offsetY: sourceOffset.y + 1
	};
	inventory.push(item);
	updateInventory = true;
}

function getOffset(id, columns, width, height) {
	var offsetY = Math.round(Math.floor(id / columns) * height);
	var offsetX = Math.round((((id / columns) - Math.floor(id / columns)) * columns) * width);
	return {x: offsetX, y: offsetY};
}

//Setup collision response functions
function setCollisionResponses() {
	for(var i = 0; i < collisionLayers[staticLayer].length; i++) {
		var staticObj = collisionLayers[staticLayer][i];
		var checkRect = {x: staticObj.x - 1, y: staticObj.y - 1, width: staticObj.width + 2, height: staticObj.height + 2};		
		var removeLeft = false;
		var removeRight = false;
		var removeUp = false;
		var removeDown = false;
		for(var j = 0; j < collisionLayers[staticLayer].length; j++) {			
			var staticObj2 = collisionLayers[staticLayer][j];
			if(i !== j) {
				if(checkRectCollision(checkRect, staticObj2)) {					
					if(staticObj.y === staticObj2.y && staticObj.height === staticObj2.height) {
						//staticObj2 is a parallel height
						if(staticObj.x < staticObj2.x) {
							//There is a barrier to the left
							removeLeft = true;
						} else {
							//There is a barrier to the right
							removeRight = true;
						}
					} else if(staticObj.x === staticObj2.x && staticObj.width === staticObj2.width) {
						//staticObj2 is a parallel width
						if(staticObj.y > staticObj2.y) {
							//There is a barrier below
							removeDown = true;
						} else {
							//There is a barrier above
							removeUp = true;
						}
					}
				}
			}
		}
		if(removeLeft) {
			staticObj.collisionResponses.splice(staticObj.collisionResponses.indexOf(checkLeft), 1);
		}
		if(removeRight) {
			staticObj.collisionResponses.splice(staticObj.collisionResponses.indexOf(checkRight), 1);
		}
		if(removeUp) {
			staticObj.collisionResponses.splice(staticObj.collisionResponses.indexOf(checkUp), 1);
		}
		if(removeDown) {
			staticObj.collisionResponses.splice(staticObj.collisionResponses.indexOf(checkDown), 1);
		}
	}
}

function checkLoaded(src, img) {
	imagesLoaded[src] = false;
	img.onload = function () {
		imagesLoaded[src] = true;
	}		
}

function createObj(x, y, width, height, drawMethod, objType, img, imageWidth, imageHeight, offsetX, offsetY) {
	var obj = {
		id: lastId++,
		x: x,
		y: y,		
		width: width,
		height: height,
		angle: 0,
		draw: drawMethod,
		color: "white",
		objType: objType,
		img: img,
		imageWidth: imageWidth,
		imageHeight: imageHeight,
		offsetX: offsetX,
		offsetY: offsetY,
		collisionResponses: [checkLeft, checkRight, checkUp, checkDown]
	};
	
	if(objType === "dynamic") {		
		obj.vx = 0;
		obj.vy = 0;
		obj.speed = 0.2;
		obj.flightSpeed = 0.20;
		obj.normalSpeed = 0.3;
		obj.runSpeed = 0.5;
		obj.jetForce = 0.00075;
		obj.jump = false;
		obj.moveLeft = false;
		obj.moveRight = false;
		obj.useItem = false;
		obj.useMap = false;
		obj.grounded = false;
		obj.collisionLayers = [collisionLayers[staticLayer], collisionLayers[dynamicLayer]];
		obj.move = moveObject;
		collisionLayers[dynamicLayer].push(obj);
		allObjects.push(obj);
	} else if(objType === "static") {
		obj.collisionLayers = [];
		obj.move = function () {};
		collisionLayers[staticLayer].push(obj);
		allObjects.push(obj);
	} else if(objType === "background") {
		obj.move = function () { };
		backgroundObjects.push(obj);
	} else if(objType === "foreground" ) {
		obj.move = function () { };
		foregroundObjects.push(obj);
	}
	
	return obj;
}

function createTrigger(x, y, width, height, triggerOn, triggerOff, _collisionLayers) {
	var obj = createObj(x, y, width, height, drawRect, "");
	if(_collisionLayers) {
		obj.collisionLayers = _collisionLayers;
	} else {
		obj.collisionLayers = [collisionLayers[staticLayer], collisionLayers[dynamicLayer]];
	}
	obj.triggered = false;
	obj.triggerOn = triggerOn;
	obj.triggerOff = triggerOff;
		
	triggers.push(obj);
	return obj;
}

function setTracker(obj, tracker, relX, relY, move) {
	obj.tracker = tracker;
	obj.relX = relX;
	obj.relY = relY;
	obj.move = move;
	trackers.push(obj);				
}

function drawRect() {	
	context.fillStyle = this.color;
	context.translate(this.x, this.y);
	context.rotate(this.angle);
	context.fillRect(0, 0, this.width, this.height);
	context.rotate(-this.angle);
	context.translate(-this.x, -this.y);
}

function drawImage() {	
	context.translate(this.x, this.y);
	context.rotate(this.angle);
	context.drawImage(this.img, this.offsetX, this.offsetY, this.imageWidth, this.imageHeight, 0, 0, this.width, this.height);
	context.rotate(-this.angle);
	context.translate(-this.x, -this.y);
}

function drawSprite() {
	if(this.animationFrame >= this.animation.length) {
		if(this.animationRepeat) {
			this.animationFrame = 0;	
		} else {
			this.animationFrame -= 1;
		}
	}
	var row = Math.floor(this.animation[this.animationFrame] / this.img.spriteSize.framesPerRow);
	var col = Math.floor(this.animation[this.animationFrame] % this.img.spriteSize.framesPerRow);
	
	var sourceX = col * this.img.spriteSize.x;	
	var sourceY = row * this.img.spriteSize.y;
	
	context.translate(this.x, this.y);
	context.rotate(this.angle);	
	
	context.scale(this.animationDirection, 1);	
	var offsetX2 = this.offsetX;
	if(this.animationDirection < 0) {
		offsetX2 = this.offsetX - this.width;
	}
	context.drawImage(this.img, sourceX, sourceY, this.img.spriteSize.x, this.img.spriteSize.y, offsetX2, this.offsetY, this.imageWidth, this.imageHeight);
	context.scale(this.animationDirection, 1);
	//context.fillStyle = "rgba(255, 0, 255, 0.25)";
	//context.fillRect(0, 0, this.width, this.height);
	context.rotate(-this.angle);
	context.translate(-this.x, -this.y);
	
	if(lastTime > this.animationLast + this.animationDelay) {		
		this.animationFrame++;
		this.animationLast = lastTime;		
		//console.log(this.animation[this.animationFrame]);
	}
}

function drawParticles() {
	/*{
		x: gameObj.x + Math.floor(Math.random() * 6) - 3,
		y: gameObj.y + Math.floor(Math.random() * 6) - 3,
		vx: Math.floor(Math.random() * 8),
		vy: Math.floor(Math.random() * 8),
		color: colorChoices[Math.floor(Math.random() * colorChoices.length)]
	}*/
	context.save();
	for(var i = 0; i < this.particles.length; i++) {
		var pt = this.particles[i];
		//context.globalAlpha = Math.min((pt.life / pt.maxLife) + 0.5, 1);
		
		context.fillStyle = pt.color;
		context.translate(pt.x, pt.y);
		context.rotate(pt.angle);
		//context.fillRect(0, 0, pt.width, pt.height);
		context.drawImage(this.img, -pt.width / 2, -pt.height / 2, pt.width, pt.height);
		context.rotate(-pt.angle);
		context.translate(-pt.x, -pt.y);
		//context.globalAlpha = 1;
		pt.move();
		//pt.angle += pt.spin;
		pt.life -= dt;
		if(pt.life < 0) {
			pt.reset();
		}
	}
	context.restore();
}

function run() {
	updateTime();
	
	context.clearRect(0, 0, canvas.width, canvas.height);		
	context.translate(-Math.round(camera.x), -Math.round(camera.y));	
		
	//Draw background objects
	for(var i = 0; i < backgroundObjects.length; i++) {
		backgroundObjects[i].draw();
	}
		
	//Move, detect collisions, and draw
	for(var i = 0; i < allObjects.length; i++) {
		var obj = allObjects[i];
		obj.move();
		for(var j = 0; j < obj.collisionLayers.length; j++) {
			detectCollisions(obj, obj.collisionLayers[j]);
		}
		obj.draw();
	}
	
	//Move and draw trackers
	for(i = 0; i < trackers.length; i++) {
		trackers[i].move();		
		//trackers[i].draw();	
	}
	
	//Detect collisions with triggers
	for(var i = 0; i < triggers.length; i++) {
		var trigger = triggers[i];
		//trigger.draw();
		var collision = false;
		for(var j = 0; j < trigger.collisionLayers.length; j++) {
			collision = detectCollisions(trigger, trigger.collisionLayers[j]);		
			if(collision) { 
				break; 
			}
		}
		
		if(collision) {
			if(!trigger.triggered) {
				trigger.triggered = true;
				trigger.triggerOn(collision);
			}
		} else if(trigger.triggered) {
			trigger.triggered = false;
			trigger.triggerOff();
		}
	}
	
	processPlayerMovement();	
	
	//Draw foregroundObjects
	for(var i = 0; i < foregroundObjects.length; i++) {
		foregroundObjects[i].draw();
	}
	
	context.translate(Math.round(camera.x), Math.round(camera.y));	
	
	if(updateInventory) {
		drawInventory();
	}
	player.oxygen -= player.oxygenConsumption * dt;
	
	context.fillStyle = "white";
	context.fillText("Fuel: " + Math.round(player.fuel * 100) / 100, 100, 110);
	context.fillText("Oxygen: " + Math.round(player.oxygen * 100) / 100, 100, 120);
	context.fillText("FPS: " + fps, 100, 130);	
	
	updateCamera();	
	
	//if(!showingMessage) {
		timeout = requestAnimationFrame(run);	
	//}
	//document.getElementById("msg").innerHTML = "speed:" + player.speed;
}

function drawInventory() {
		
	var size = 64;
	var numSize = 26;
	var totalSize = size * 10;
	var start = canvasUI.width / 2 - totalSize / 2;
		
	contextUI.clearRect(0, 0, canvasUI.width, canvasUI.height);
	contextUI.lineWidth = 1;
	
	//Draw Inventory Items
	contextUI.globalAlpha = 0.8;
	for(var i = 0; i < inventory.length; i++) {		
		var item = inventory[i];				
		contextUI.drawImage(item.img, item.offsetX + 1, item.offsetY, item.sourceWidth, item.sourceHeight, Math.round(start + i * size + size * 0.125) + 0.5, Math.round(size * 0.125) + 5.5, Math.round(size * 0.75), Math.round(size * 0.75));		
	}
	contextUI.globalAlpha = 1;
	
	//Draw boxes	
	contextUI.strokeStyle = "#ececec";
	for(var i = 0; i < 9; i++) {
		contextUI.strokeRect(Math.round(start + i * size) + 0.5, 5.5, size, size);
	}
	
	contextUI.font = numSize + "px Tahoma, Geneva, sans-serif";
	//Draw Numbers
	for(var i = 0; i < 9; i++) {
		//contextUI.fillStyle = "black";
		//contextUI.fillRect(start + i * size, 0, numSize, numSize);
		if(i === selectedItem) {
			contextUI.strokeStyle = "rgb(51, 102, 51)";
			contextUI.fillStyle = "rgb(51, 200, 51)";
		} else {			
			contextUI.fillStyle = "#ececec";
			contextUI.strokeStyle = "#ececec";
		}
		contextUI.fillText((i + 1), start + i * size + numSize / 4, numSize + 2);
		contextUI.strokeRect(start + i * size + 0.5, 5.5, numSize, numSize);
	}
	
	//Draw Border around selected Item
	contextUI.strokeStyle = "rgb(51, 102, 51)";
	contextUI.lineWidth = 3;
	contextUI.strokeRect(Math.round(start + selectedItem * size) + 0.5, 5.5, size, size);
	
	updateInventory = false;	
}

function showMessage(msg, important) {
	var $msgText = $("#msgText");
	$msgText.html(msg);
	
	var $msg = $("#msg");
	if(important) {
		$msg.find("#msgImportant").show();
		$msg.find("#msgNotice").hide();
		audioPlayers[5].volume = 1;
		audioPlayers[5].play();
	} else {
		$msg.find("#msgNotice").show();
		$msg.find("#msgImportant").hide();
		audioPlayers[5].volume = 0.5;
		audioPlayers[5].play();
	}
	//$msg.css("margin-top", ($msg.height() / 2 * -1));
	var delay = msg.split(" ").length * 300;
	//$msgText.append(" " + delay);
	
	$msg.css("opacity", 1);
	$msg.show();
	$msg.stop();
	setTimeout(function () {
		$msg.animate({
			opacity: 0
		}, delay);
	}, 1000);
	showingMessage = true;
	
	//cancelAnimationFrame(timeout);
}

var fps = 0;
var frameCheck = 0;
var frameCount = 0;
var totalTime = 0;
function updateTime() {
	var t = (new Date).getTime();
	dt = t - lastTime;
	if(dt > 50) {
		dt = 50;
	}
	lastTime = t;
	
	if(t > frameCheck) {
		fps = Math.round(frameCount / totalTime * 1000 * 100) / 100;	
		frameCheck = t + 1000;
	}
	
	totalTime += dt;
	frameCount++;
}

function processPlayerMovement() {
	if(player.dead) {
		if(player.justDied) {
			player.animation = player.animationDeath;
			player.animationFrame = 0;
			player.vx = 0;
			player.justDied = false;
			audioPlayers[2].volume = 0.5;
			audioPlayers[2].play();
			audioPlayers[0].pause();
			audioPlayers[0].currentTime = 0;
			audioPlayers[1].pause();
			audioPlayers[1].currentTime = 0;
		} else {
			if(player.animationFrame >= player.animation.length) {
				player.animation = player.animationDead;
				//showMessage("You have died.");
			}
		}
	}
	else {
		if(player.grounded && player.running) {
			player.animationDelay = player.animationDelayFast;
		} else {
			player.animationDelay = player.animationDelayNormal;
		}
		if(player.moveLeft)	{
			player.animationDirection = -1;
			if(player.grounded) {
				if(player.running) {
					player.animation = player.animationRunning;
					player.speed = player.runSpeed;
				} else {
					player.animation = player.animationWalking;	
					player.speed = player.normalSpeed;
				}
				audioPlayers[1].volume = 0.15;
				audioPlayers[1].play();
			} else {				
				player.animation = player.animationFlyingForward;
				audioPlayers[0].volume = 0.15;
				audioPlayers[0].play();				
				player.fuel -= player.secondaryFuelConsumption * dt;			
			}
			player.vx = -player.speed;
		} else if(player.moveRight) {
			player.animationDirection = 1;
			if(player.grounded) {
				if(player.running) {					
					player.animation = player.animationRunning;
					player.speed = player.runSpeed;
				} else {
					player.animation = player.animationWalking;
					player.speed = player.normalSpeed;
				}
				audioPlayers[1].volume = 0.15;
				audioPlayers[1].play();
			} else {
				player.animation = player.animationFlyingForward;
				audioPlayers[0].volume = 0.15;
				audioPlayers[0].play();
				player.fuel -= player.secondaryFuelConsumption * dt;
			}
			player.vx = player.speed;
		} else {
			player.vx = 0;		
			if(player.grounded) {
				player.animation = player.animationIdle;		
			} else {
				player.animation = player.animationFalling;
				if(player.vy === 0 && !player.jump) {
					player.vx = player.animationDirection * player.speed;
				}
			}
			audioPlayers[1].pause();
			audioPlayers[1].currentTime = 0;
		}
		
		if(player.jump) {
			player.vy = Math.max(player.vy - (player.jetForce * dt), -0.5);
			if(player.animation !== player.animationFlyingForward) {
				player.animation = player.animationFlying;
			}
			player.fuel -= player.mainFuelConsumption * dt;
			audioPlayers[0].volume = 0.25;
			audioPlayers[0].play();
	//		console.log(player.vy);
		} else if(player.grounded || player.animation === player.animationFalling) {			
			audioPlayers[0].pause();
			audioPlayers[0].currentTime = 0;
		}
		
		if(player.useItem) {
			if(selectedItem < inventory.length) {
				var item = inventory[selectedItem].obj;
				if(item.properties && item.properties["Unlocks"]) {
					var itemTarget = mapObjects[item.properties["Unlocks"]].gameObj;
					if(!itemTarget.used) {
						var itemX = itemTarget.x + itemTarget.width / 2;
						var playerX = player.x + player.width / 2;
						var itemY = itemTarget.y + itemTarget.height / 2;
						var playerY = player.y + player.height / 2;
						
						var dx = itemX - playerX;
						var dy = itemY - playerY;
						var d = Math.sqrt(dx * dx + dy * dy);
						if(d < player.width * 3) {
							itemTarget.height = 5;
							itemTarget.used = true;
							audioPlayers[4].play();
						}
					}
				}
			}
		}
	}
}

function moveObject() {
	this.lastX = this.x;
	this.lastY = this.y;
	
	this.x += this.vx * dt;
	this.y += this.vy * dt;
	this.vy += gravity * dt;
}

function detectCollisions(obj, objList) {	
	var collision = false;
	for(var i = 0; i < objList.length; i++)	{		
		var obj2 = objList[i];
		if(checkRectCollision(obj, obj2)) {
			collision = obj2;
			for(var j = 0; j < obj2.collisionResponses.length; j++) {
				obj2.collisionResponses[j](obj, obj2);
			}
			//checkRight(obj, obj2);
			//checkLeft(obj, obj2);
			//checkDown(obj, obj2);
			//checkUp(obj, obj2);
		}	
	}	
	
	return collision;
}

function checkRight(obj1, obj2) {
	if(obj1.lastX + obj1.width <= obj2.x && obj1.x + obj1.width > obj2.x) {			
		obj1.x -= (obj1.x + obj1.width) - obj2.x;
		obj1.vx = 0;	
	}
}

function checkLeft(obj1, obj2) {
	if(obj1.lastX >= obj2.x + obj2.width && obj1.x < obj2.x + obj2.width) {
		obj1.x += (obj2.x + obj2.width) - obj1.x;
		obj1.vx = 0;				
	}
}

function checkDown(obj1, obj2) {
	if(obj1.lastY + obj1.height <= obj2.y && obj1.y + obj1.height > obj2.y) {
		obj1.y -= (obj1.y + obj1.height) - obj2.y;
		obj1.vy = 0;
	}
}

function checkUp(obj1, obj2) {
	if(obj1.lastY >= obj2.y + obj2.height && obj1.y < obj2.y + obj2.height) {
		obj1.y += (obj2.y + obj2.height) - obj1.y;
		obj1.vy = 0;				
	}
}


function checkRectCollision(rect1, rect2) {
	if(rect1.id != rect2.id) { 
		if (rect1.x < rect2.x + rect2.width &&
			rect1.x + rect1.width > rect2.x &&
			rect1.y < rect2.y + rect2.height &&
			rect1.height + rect1.y > rect2.y) {
			return true;
		}
	}
	
	return false;
}

function updateCamera() {
	if(player.x - camera.x > cameraMaxX) {
		camera.x += (player.x - camera.x) - cameraMaxX;
	} else if (player.x - camera.x < cameraMinX) {
		camera.x += (player.x - camera.x) - cameraMinX;
	}
	
	if(player.y - camera.y > cameraMaxY) {
		camera.y += (player.y - camera.y) - cameraMaxY;
	} else if (player.y - camera.y < cameraMinY) {
		camera.y += (player.y - camera.y) - cameraMinY;
	}
}

function keyDown(e) {		
	setAction(e.keyCode, true);
}

function keyUp(e) {
	//document.getElementById("stats").innerHTML = e.keyCode;
	setAction(e.keyCode, false);
}

function setAction(keyCode, bOn) {		
	switch(keyCode) {
		//Left
		case 65:
		case 37:
		case 100:
			player.moveLeft = bOn;	
			//pacman.moveRight = false;
			break;
		//Right
		case 68:
		case 39:
		case 102:
			player.moveRight = bOn;			
			//pacman.moveLeft = false;
			break;
		//Up
		case 87:
		case 38:
		case 104:
			player.jump = bOn;	
			//.moveDown = false;
			break;	
		//Down
		case 40:
		case 83:
			//player.moveDown = bOn;			
			//pacman.moveUp = false;
			break;	
		case 49:
		case 50:
		case 51:
		case 52:
		case 53:
		case 54:
		case 55:
		case 56:
		case 57:
			if(bOn) {
				selectedItem = keyCode - 49;
				updateInventory = true;
			}
			break;
		case 70:
			player.useItem = bOn;
			break;
		case 71:
			player.useMap = bOn;
			break;
		case 13:
			//if(showingMessage) {
			//	$("#btnOK").trigger("click");
			//}
			break;
		case 16: 
			player.running = bOn;
			break;
	}
}