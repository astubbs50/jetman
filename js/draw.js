"use strict";

var canvas;
var context;
var canvasLight;
var contextLight;
var canvasUI;
var contextUI;
var canvasForeground;
var contextForeground;
var drawLayers;
var darkness = 0;
var lights = [];
var masks = [];

function initGraphics() {
	canvas = document.getElementById("myCanvas");	
	context = canvas.getContext("2d");
	canvasUI = document.getElementById("myCanvasUI");
	contextUI = canvasUI.getContext("2d");
	
	canvasLight = document.getElementById("myCanvasLight");
	//canvasLight.style.opacity = "0.25";
	contextLight = canvasLight.getContext("2d");
	canvasForeground = document.getElementById("myCanvasForeground");
	contextForeground = canvasForeground.getContext("2d");
	
	context.imageSmoothingEnabled = false;
	contextUI.imageSmoothingEnabled = false;	
	contextLight.imageSmoothingEnabled = false;	
	contextForeground.imageSmoothingEnabled = false;	
	
	drawLayers = {
		"background" : {
			context: context,
			objects: []
		}, "bullets" : {
			context: context,
			objects: [],
		}, "main": {
			context: context,
			objects: [],
		}, "foreground": {
			context: contextForeground,
			objects: [],
		}
	};
	
	resizeCanvas();
	context.fillStyle = "white";	
	backgroundImage = new Image();
	backgroundImage.src = "img/level_01_background_04.png";	
	checkLoaded(backgroundImage.src, backgroundImage);
}

function resizeCanvas() {
	var canvasHeight;
	var canvasWidth;
	
	if(window.innerWidth > window.innerHeight) {
		var aspect = aspectY / aspectX;
		canvasHeight = window.innerWidth * aspect;
		canvasWidth = window.innerWidth;
		
		canvasUI.style.height = window.innerHeight + "px";
		canvasUI.style.width = (window.innerHeight * (aspectX / aspectY)) + "px";		
		canvasUI.style.left = (window.innerWidth - (window.innerHeight * (aspectX / aspectY))) / 2 + "px";
	} else {
		var aspect = aspectX / aspectY;
		canvasHeight = window.innerHeight;
		canvasWidth = window.innerHeight * aspect;
		
		canvasUI.style.height = (window.innerWidth * (aspectY / aspectX)) + "px";
		canvasUI.style.width = window.innerWidth + "px";
		canvasUI.style.top = "0px";
	}
	
	var diffY = ((canvasHeight - window.innerHeight) / 2);
	var diffGameY = diffY * (gameY / canvasHeight);
	cameraMaxY = gameY - (diffGameY + 200);
	cameraMinY = (diffGameY + 200);	
	canvas.style.height = canvasHeight + "px";
	canvas.style.top = -diffY + "px";	

	var diffX = ((canvasWidth - window.innerWidth) / 2);
	var diffGameX = diffX * (gameX / canvasWidth);
	cameraMaxX = gameX - (diffGameX + 400);
	cameraMinX = (diffGameX + 400);	
	canvas.style.width = canvasWidth + "px";
	canvas.style.left = -diffX + "px";
	
	var canvasBack = document.getElementById("myCanvasBack");
	canvasBack.style.height = canvasHeight + "px";
	canvasBack.style.width = canvasWidth + "px";	
	canvasBack.style.left = -diffX + "px";
	canvasBack.style.top = -diffY + "px";	
	
	canvas.ratioX = canvas.width / canvas.offsetWidth;
	canvas.ratioY = canvas.height / canvas.offsetHeight;
	
	canvasLight.style.height = canvasHeight + "px";
	canvasLight.style.width = canvasWidth + "px";	
	canvasLight.style.left = -diffX + "px";
	canvasLight.style.top = -diffY + "px";	
	
	canvasForeground.style.height = canvasHeight + "px";
	canvasForeground.style.width = canvasWidth + "px";	
	canvasForeground.style.left = -diffX + "px";
	canvasForeground.style.top = -diffY + "px";	
}


function drawRect(context) {	
	context.fillStyle = this.color;
	context.translate(this.x, this.y);
	context.rotate(this.angle);
	context.fillRect(0, 0, this.width, this.height);
	context.rotate(-this.angle);
	context.translate(-this.x, -this.y);
}

function drawImage(context) {	
	context.translate(this.x, this.y);
	context.rotate(this.angle);
	context.drawImage(this.img, this.offsetX, this.offsetY, this.imageWidth, this.imageHeight, 0, 0, this.width, this.height);
	context.rotate(-this.angle);
	context.translate(-this.x, -this.y);
}

function drawSprite(context) {
	if(this.animationFrame >= this.animation.length) {
		if(this.animationRepeat) {
			this.animationFrame = 0;	
		} else {
			this.animationFrame -= 1;
		}
	}
	var row = Math.floor(this.animation[this.animationFrame] / this.img.spriteSize.framesPerRow);
	var col = Math.floor(this.animation[this.animationFrame] % this.img.spriteSize.framesPerRow);
	
	var sourceX = col * this.img.spriteSize.x;	
	var sourceY = row * this.img.spriteSize.y;
	context.save();
	context.translate(this.x, this.y);
	context.rotate(this.angle);	
	
	context.scale(this.animationDirection, 1);	
	var offsetX2 = this.offsetX;
	if(this.animationDirection < 0) {
		offsetX2 = this.offsetX - this.width;
	}
	context.drawImage(this.img, sourceX, sourceY, this.img.spriteSize.x, this.img.spriteSize.y, offsetX2, this.offsetY, this.imageWidth, this.imageHeight);
	context.scale(this.animationDirection, 1);
	
	//context.fillStyle = "rgba(255, 0, 255, 0.25)";
	//context.fillRect(0, 0, this.width, this.height);
	
	context.rotate(-this.angle);
	context.translate(-this.x, -this.y);
	context.restore();
	if(lastTime > this.animationLast + this.animationDelay) {		
		this.animationFrame++;
		this.animationLast = lastTime;		
		//console.log(this.animation[this.animationFrame]);
	}
}

function drawParticles(context) {
	/*{
		x: gameObj.x + Math.floor(Math.random() * 6) - 3,
		y: gameObj.y + Math.floor(Math.random() * 6) - 3,
		vx: Math.floor(Math.random() * 8),
		vy: Math.floor(Math.random() * 8),
		color: colorChoices[Math.floor(Math.random() * colorChoices.length)]
	}*/
	context.save();
	for(var i = 0; i < this.particles.length; i++) {
		var pt = this.particles[i];
		//context.globalAlpha = Math.min((pt.life / pt.maxLife) + 0.5, 1);
		
		context.fillStyle = pt.color;
		context.translate(pt.x, pt.y);
		context.rotate(pt.angle);
		//context.fillRect(0, 0, pt.width, pt.height);
		context.drawImage(this.img, -pt.width / 2, -pt.height / 2, pt.width, pt.height);
		context.rotate(-pt.angle);
		context.translate(-pt.x, -pt.y);
		//context.globalAlpha = 1;
		pt.move();
		//pt.angle += pt.spin;
		pt.life -= dt;
		if(pt.life < 0) {
			pt.reset();
		}
	}
	context.restore();
}



function drawInventory() {
		
	var size = 64;
	var numSize = 26;
	var totalSize = size * 10;
	var start = canvasUI.width / 2 - totalSize / 2;
		
	contextUI.clearRect(0, 0, canvasUI.width, canvasUI.height);
	contextUI.lineWidth = 1;
	
	//Draw Inventory Items
	contextUI.globalAlpha = 0.8;
	for(var i = 0; i < inventory.length; i++) {		
		var item = inventory[i];				
		contextUI.drawImage(item.img, item.offsetX + 1, item.offsetY, item.sourceWidth, item.sourceHeight, Math.round(start + i * size + size * 0.125) + 0.5, Math.round(size * 0.125) + 5.5, Math.round(size * 0.75), Math.round(size * 0.75));		
	}
	contextUI.globalAlpha = 1;
	
	//Draw boxes	
	contextUI.strokeStyle = "#ececec";
	for(var i = 0; i < 9; i++) {
		contextUI.strokeRect(Math.round(start + i * size) + 0.5, 5.5, size, size);
	}
	
	contextUI.font = numSize + "px Tahoma, Geneva, sans-serif";
	//Draw Numbers
	for(var i = 0; i < 9; i++) {
		//contextUI.fillStyle = "black";
		//contextUI.fillRect(start + i * size, 0, numSize, numSize);
		if(i === selectedItem) {
			contextUI.strokeStyle = "rgb(51, 102, 51)";
			contextUI.fillStyle = "rgb(51, 200, 51)";
		} else {			
			contextUI.fillStyle = "#ececec";
			contextUI.strokeStyle = "#ececec";
		}
		contextUI.fillText((i + 1), start + i * size + numSize / 4, numSize + 2);
		contextUI.strokeRect(start + i * size + 0.5, 5.5, numSize, numSize);
	}
	
	//Draw Border around selected Item
	contextUI.strokeStyle = "rgb(51, 102, 51)";
	contextUI.lineWidth = 3;
	contextUI.strokeRect(Math.round(start + selectedItem * size) + 0.5, 5.5, size, size);
	
	updateInventory = false;	
}


function checkLoaded(src, img) {
	imagesLoaded[src] = false;
	img.onload = function () {
		imagesLoaded[src] = true;
	}		
}

function createSprite(fileName) {
	var img = new Image();
	img.src = fileName;
	img.spriteSize = { x: 34, y: 34, framesPerRow: 4 };
		
	var sprite = {
		img: img,
		draw: drawSprite,
		animationFrame: 0,
		animation: [0],
		animationDirection: 0,
		animationLast: 0,
		animationDelay:50,
		animationRepeat: false,
		x: 0,
		y: 0,
		angle: 0,
		offsetX: 0,
		offsetY: 0,
		width: 0,
		height: 0,
		imageWidth: 0,
		imageHeight: 0		
	};
	
	checkLoaded(img.src, img);
	
	return sprite;
}

function createLight(obj) {
	obj.width /= 2;
	var gradient = contextLight.createRadialGradient(
		obj.width,
		obj.width,
		obj.width,
		obj.width,		
		obj.width,
		obj.width * .9);
		
	gradient.addColorStop(0, "rgba(0,0,0,0)");
	//gradient.addColorStop(0.95, "rgba(0,0,0,1)");
	gradient.addColorStop(1, "rgba(0,0,0,1)");
	var light = {
		obj: obj,
		x: obj.x,
		y: obj.y,
		gradient: gradient,
		size: obj.width * 2
	};
	lights.push(light);
	
	return light;
}

function drawLights() {
	contextLight.save();
	contextLight.clearRect(0, 0, canvasLight.width, canvasLight.height);
	//contextLight.fillStyle = "black";
	//contextLight.fillRect(0, 0, canvasLight.width, canvasLight.height);		
	
	contextLight.translate(-Math.round(camera.x), -Math.round(camera.y));	
	
	//draw background masks		
	contextLight.globalCompositeOperation = "source-over";
	for(var j = 0; j < masks.length; j++) {
		drawMask(masks[j]);
	}	
	
	contextLight.translate(Math.round(camera.x), Math.round(camera.y));
	contextLight.globalCompositeOperation = "xor";
	contextLight.fillStyle = "black";
	contextLight.fillRect(0, 0, canvasLight.width, canvasLight.height);		
	contextLight.translate(-Math.round(camera.x), -Math.round(camera.y));	
	
	contextLight.globalCompositeOperation = "source-over";
	for(var i = 0; i < lights.length; i++) {
		//contextLight.save();
		contextLight.translate(lights[i].x, lights[i].y);
		contextLight.fillStyle = lights[i].gradient;
		//contextLight.beginPath();
		//contextLight.arc(lights[i].size / 2, lights[i].size / 2, lights[i].obj.properties["innerRadius"], 0, Math.PI * 2, true);
		//contextLight.arc(lights[i].size / 2, lights[i].size / 2, lights[i].size, 0, Math.PI * 2, false);
		//contextLight.clip();
		//contextLight.closePath();
		contextLight.fillRect(0, 0, lights[i].size, lights[i].size);
		contextLight.translate(-lights[i].x, -lights[i].y);
		//contextLight.restore();
	}
	
	contextLight.translate(Math.round(camera.x), Math.round(camera.y));
	contextLight.globalCompositeOperation = "xor";		
	contextLight.fillStyle = "black";
	contextLight.fillRect(0, 0, canvasLight.width, canvasLight.height);
	contextLight.restore();
}

function drawMask(obj) {
	
	contextLight.translate(obj.x, obj.y);
	contextLight.rotate(obj.angle);
	contextLight.drawImage(tileSpriteSheetMask, obj.offsetX, obj.offsetY, obj.imageWidth, obj.imageHeight, 0, 0, obj.width - 1, obj.height - 1);
	contextLight.rotate(-obj.angle);
	contextLight.translate(-obj.x, -obj.y);
}

function draw() {
	context.clearRect(0, 0, canvas.width, canvas.height);		
	context.translate(-Math.round(camera.x), -Math.round(camera.y));	
	contextForeground.clearRect(0, 0, canvas.width, canvas.height);		
	contextForeground.translate(-Math.round(camera.x), -Math.round(camera.y));	
	
	for(var i in drawLayers) {
		var drawLayer = drawLayers[i];
		for(var j = 0; j < drawLayer.objects.length; j++) {
			drawLayer.objects[j].draw(drawLayer.context);
		}
	}
	
	context.translate(Math.round(camera.x), Math.round(camera.y));	
	contextForeground.translate(Math.round(camera.x), Math.round(camera.y));	
	
	contextForeground.fillStyle = "white";
	contextForeground.fillText("Fuel: " + Math.round(player.fuel * 100) / 100, 100, 110);
	contextForeground.fillText("Oxygen: " + Math.round(player.oxygen * 100) / 100, 100, 120);
	contextForeground.fillText("FPS: " + fps, 100, 130);	
	
	drawLights();
	
	if(updateInventory) {
		drawInventory();
	}
}











