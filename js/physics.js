
function moveObject() {
	this.lastX = this.x;
	this.lastY = this.y;
	
	this.x += this.vx * dt;
	this.y += this.vy * dt;
	this.vy += gravity * dt;
}

function moveBullet() {
	this.lastX = this.x;
	this.lastY = this.y;
	
	this.x += this.vx * dt;
	this.y += this.vy * dt;
}

function detectCollisions(obj, objList) {	
	var collision = false;
	for(var i = 0; i < objList.length; i++)	{		
		var obj2 = objList[i];
		if(checkRectCollision(obj, obj2)) {
			collision = obj2;
			for(var j = 0; j < obj2.collisionResponses.length; j++) {
				obj2.collisionResponses[j](obj, obj2);
			}
			//checkRight(obj, obj2);
			//checkLeft(obj, obj2);
			//checkDown(obj, obj2);
			//checkUp(obj, obj2);
		}	
	}	
	
	return collision;
}

function checkRight(obj1, obj2) {
	if(obj1.lastX + obj1.width <= obj2.x && obj1.x + obj1.width > obj2.x) {			
		obj1.x -= (obj1.x + obj1.width) - obj2.x;
		obj1.vx = 0;	
	}
}

function checkLeft(obj1, obj2) {
	if(obj1.lastX >= obj2.x + obj2.width && obj1.x < obj2.x + obj2.width) {
		obj1.x += (obj2.x + obj2.width) - obj1.x;
		obj1.vx = 0;				
	}
}

function checkDown(obj1, obj2) {
	if(obj1.lastY + obj1.height <= obj2.y && obj1.y + obj1.height > obj2.y) {
		obj1.y -= (obj1.y + obj1.height) - obj2.y;
		obj1.vy = 0;
	}
}

function checkUp(obj1, obj2) {
	if(obj1.lastY >= obj2.y + obj2.height && obj1.y < obj2.y + obj2.height) {
		obj1.y += (obj2.y + obj2.height) - obj1.y;
		obj1.vy = 0;				
	}
}


function checkRectCollision(rect1, rect2) {
	if(rect1.id != rect2.id) { 
		if (rect1.x < rect2.x + rect2.width &&
			rect1.x + rect1.width > rect2.x &&
			rect1.y < rect2.y + rect2.height &&
			rect1.height + rect1.y > rect2.y) {
			return true;
		}
	}
	
	return false;
}